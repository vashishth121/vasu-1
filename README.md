## Hi there, I'm Vashishth Patel - [Competitive Programmer][website] 

[![Website](https://shields.io/badge/Portfolio-up-blue?style=flat-square)](https://vashishth.epizy.com/?i=1)
[![Linkedin Follow](https://shields.io/badge/Follow%20@Vashishth%20Patel-921-green?logo=linkedin&style=flat-square)](https://www.linkedin.com/in/vashishth-patel-312a52204/)
![](https://komarev.com/ghpvc/?username=vasu-1&style=flat-square)


---

### CS Student At @DDIT

- 🔭 Looking for an internship in Software Development.
- 🌱 Like to work with New People🧑‍🤝‍🧑.
- 🥅 2021 Goals: Contribute to open-source projects.
- 👯 I’m mainly into Data structures and algorithms.
- 🖥️ currently learning Web Development.

[website]: https://vashishth.epizy.com/?i=1
[facebook]: https://www.facebook.com/people/Vashishth-Patel/100071806075318/
[youtube]: https://www.youtube.com/channel/UCT_aAHVTwIPvW3mEUfHbB7g
[instagram]: https://www.instagram.com/vashishthchaudhary/
[linkedin]: https://www.linkedin.com/in/vashishth-patel-312a52204/
[askubuntu]: https://askubuntu.com/users/1357742/vashishth-patel
[codechef]: https://www.codechef.com/users/vashishth48
[hackerrank]: https://www.hackerrank.com/vashishthchaudh1
[github]: https://www.github.com/vasu-1
